#!/bin/bash
# Prepares LateX sources by unfolding the input/include graph and removing the comments
# Useful for uploading Latex sources to Arxiv for eternity
# Usage: preprocess_latex article.tex > cleaned.tex
# Author: Martin Monperrus

if [[ -z $1 ]];
then
 echo usage: `basename $0` article.tex
 exit
fi

function preprocess_latex {

  # remove the comments (take care of the escaped percent
  # stop sed if \endinput is encountered
  cat $1 | sed -r -e '/^[[:space:]]*%/ d' -e 's/([^\\])%.*$/\1/'  -e '$a\\n' -e 's/\\endinput/%endinput/' -e '/%endinput/ q' | \
  
  # -r disable interpretation of backslash escapes which "can" be found in Latex documents ;-)
  while read -r
  do
    if [[ $REPLY =~ input || $REPLY =~ include ]]
      then
      file=`echo $REPLY | perl -ne 'if(/^[^%]*(?:input|include)\{(.*?)\}/) {print $1};'`
      
      # tex allows filenames without .tex
      if [[ ! -f $file ]];
      then
        file=$file.tex
      fi
      
      # now the recursion
      if [[ -f $file ]];
      then
        echo "% $REPLY"
        preprocess_latex $file
      else   
        echo "$REPLY"
      fi
    
    
    else
       echo "$REPLY"
    fi
    
  done
}

preprocess_latex $1